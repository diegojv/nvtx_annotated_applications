# GPU MPI+OpenMP offload prescriptive implementation

- To compile use the provided Makefile. Make sure to substitute the MPI directories in the file. 
- Execution examples are provided in: executionSingleGPU.slurm and executionMultiGPU.slurm 
