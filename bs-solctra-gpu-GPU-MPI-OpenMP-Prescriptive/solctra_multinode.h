#ifndef SOLCTRA_SOLCTRA_H
#define SOLCTRA_SOLCTRA_H

#include <string>
#include <cmath>
#include "utils.h"
#include <sstream>
#include <mpi.h>

void load_coil_data(double* coils, const std::string& path);
void e_roof(double* coils, double* e_r, double* leng_segment);
void printeroof(GlobalData& data, const int subsetIndex, const std::string& outputPath);
cartesian magnetic_field(double*coils,double* e_r,double* leng_segment, double* point);
double* initializeGlobals();
void finishGlobals(double* rmi_f);
bool computeIteration(double* coils,double* e_r,double* leng_segment, double* start_point, const double& step_size, const int mode,int &divergenceCounter);
double norm_of(double x,double y,double z);
void printParallelIterationFile(double* particle_array, const int iteration, const std::string& output, const int rank_id, const int length, const int offset);
void printIterationFileTxt(double* particle_array, const int iteration, const std::string& output,const int length);
void runParticles(double* coils, double* e_r, double* leng_segment, const std::string& output, double* particles, const int length, const int steps, const double step_size, const int mode, const int debugFlag);
    
#endif //SOLCTRA_SOLCTRA_H
