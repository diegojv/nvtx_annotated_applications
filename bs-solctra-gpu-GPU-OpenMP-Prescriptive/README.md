# GPU OpenMP offload prescriptive implementation

- To compile use the provided Makefile. Select the appropiate compiler depending on your platform
- Execution examples are provided in: singleGPUSmall.slurm, singleGPUMedium.slurm singleGPULarge.slurm
