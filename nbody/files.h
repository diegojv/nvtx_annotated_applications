#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <string>
#include <sstream>
#include <cstring>

void read_values_from_file(const char * file, float * data, size_t size) {
    std::ifstream values(file, std::ios::binary);
    values.read(reinterpret_cast<char*>(data), size);
    values.close();
}

void write_values_to_file(const char * file, float * data, size_t size) {
    std::ofstream values(file, std::ios::binary);
    values.write(reinterpret_cast<char*>(data), size);
    values.close();
}


void write_iteration_to_file(const int iteration, float * data, size_t size) {
    std::ostringstream indexString;
    indexString << iteration;
    std::string valueIndex = indexString.str(); 
    std::string file_name = "files/iteration_" + valueIndex;
    std::ofstream values(file_name, std::ios::binary);
    values.write(reinterpret_cast<char*>(data), size);
    values.close();
}
