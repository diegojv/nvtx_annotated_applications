#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "files.h"
#include "nvToolsExt.h"

#define SOFTENING 1e-9f

/*
 * Each body contains x, y, and z coordinate positions,
 * as well as velocities in the x, y, and z directions.
 */

typedef struct { float x, y, z, vx, vy, vz; } Body;

/*
 * Calculate the gravitational impact of all bodies in the system
 * on all others.
 */


__global__ void bodyForce(Body *p, float dt, int n) {

  int index = threadIdx.x + blockIdx.x * blockDim.x;
  int stride = blockDim.x * gridDim.x;
  for (int i = index; i < n; i+=stride) {
    float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

    for (int j = 0; j < n; j++) {
      float dx = p[j].x - p[i].x;
      float dy = p[j].y - p[i].y;
      float dz = p[j].z - p[i].z;
      float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
      float invDist = rsqrtf(distSqr);
      float invDist3 = invDist * invDist * invDist;

      Fx += dx * invDist3; Fy += dy * invDist3; Fz += dz * invDist3;
    }

    p[i].vx += dt*Fx; p[i].vy += dt*Fy; p[i].vz += dt*Fz;
  }
}


__global__ void integratePositions(Body *p, float dt, int n){

    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int stride = blockDim.x * gridDim.x;
    for (int i = index ; i < n; i+=stride) { // integrate position
      p[i].x += p[i].vx*dt;
      p[i].y += p[i].vy*dt;
      p[i].z += p[i].vz*dt;
    }

}



void computeNBody11(Body *p, float dt, int n, size_t threadsPerBlock, size_t numberOfBlocks){
    nvtxRangePushA("computeNBody11"); //NVTX_ANNOTATION 
    cudaError_t asyncErr;
    cudaError_t kernelErr, syncErr;
    
    nvtxRangePushA("bodyForce"); //NTVX_ANNOTATION
    bodyForce<<<numberOfBlocks,threadsPerBlock>>>(p, dt, n); // compute interbody forces
    nvtxRangePop(); //NVTX_ANNOTATION
     
    nvtxRangePushA("cudaGetLastError"); //NVTX_ANNOTATION
    kernelErr = cudaGetLastError();
    nvtxRangePop(); //NVTX_ANNOTATION
    if(kernelErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(kernelErr));
    
    nvtxRangePushA("cudaDeviceSynchronize"); //NVTX_ANNOTATION
    syncErr = cudaDeviceSynchronize();
    nvtxRangePop(); //NVTX_ANNOTATION
    if(syncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(syncErr));
  
  /*
   * This position integration cannot occur until this round of `bodyForce` has completed.
   * Also, the next round of `bodyForce` cannot begin until the integration is complete.
   */

    nvtxRangePushA("integratePositions"); //NVTX_ANNOTATION
   integratePositions<<<numberOfBlocks, threadsPerBlock>>>(p, dt, n);
   nvtxRangePop(); //NVTX_ANNOTATION

   nvtxRangePushA("cudaDeviceSynchronize"); //NVTX_ANNOTATION
   asyncErr = cudaDeviceSynchronize();
   nvtxRangePop(); //NVTX_ANNOTATION
   if(asyncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(asyncErr));
 
   nvtxRangePop(); //NVTX_ANNOTATION
    
}


void computeNBody15(Body *p, float dt, int n, size_t threadsPerBlock, size_t numberOfBlocks){
    
    nvtxRangePushA("computeNBody15"); //NVTX_ANNOTATION 
    
    cudaError_t asyncErr;
    cudaError_t kernelErr, syncErr;
    
    nvtxRangePushA("bodyForce"); //NTVX_ANNOTATION
    bodyForce<<<numberOfBlocks,threadsPerBlock>>>(p, dt, n); // compute interbody forces
    nvtxRangePop(); //NVTX_ANNOTATION
   
    nvtxRangePushA("cudaGetLastError"); //NVTX_ANNOTATION
    kernelErr = cudaGetLastError();
    nvtxRangePop(); //NVTX_ANNOTATION
    if(kernelErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(kernelErr));


    nvtxRangePushA("cudaDeviceSynchronize"); //NVTX_ANNOTATION
    syncErr = cudaDeviceSynchronize();
    nvtxRangePop(); //NVTX_ANNOTATION
    if(syncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(syncErr));
  /*
   * This position integration cannot occur until this round of `bodyForce` has completed.
   * Also, the next round of `bodyForce` cannot begin until the integration is complete.
   */

   nvtxRangePushA("integratePositions"); //NVTX_ANNOTATION
   integratePositions<<<numberOfBlocks, threadsPerBlock>>>(p, dt, n);
   nvtxRangePop(); //NVTX_ANNOTATION
   
   nvtxRangePushA("cudaDeviceSynchronize"); //NVTX_ANNOTATION
   asyncErr = cudaDeviceSynchronize();
   nvtxRangePop(); //NVTX_ANNOTATION
   if(asyncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(asyncErr));
    
    
   nvtxRangePop(); //NVTX_ANNOTATION
}


int main(const int argc, const char** argv) {

  nvtxRangePushA("main"); //NVXT_ANNOTATION
  cudaError_t mallocErr;
  size_t threadsPerBlock;
  size_t numberOfBlocks;
  int deviceId;
  int numberOfSMs;

  cudaGetDevice(&deviceId);
  cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);

  threadsPerBlock = 256;
  numberOfBlocks = 32 * numberOfSMs;


  // We want to run both problem sizes
  int nBodies_A = 2<<11;
  int nBodies_B = 2<<15;
  

  // The assessment will pass hidden initialized values to check for correctness.
  // You should not make changes to these files, or else the assessment will not work.
  const char * initialized_values_A;
  const char * solution_values_A;

  const char * initialized_values_B;
  const char * solution_values_B;

  initialized_values_A = "files/nvtx_initialized_4096";
  solution_values_A = "files/nvtx_solution_4096";
  initialized_values_B = "files/nvtx_initialized_65536";
  solution_values_B = "files/nvxt_solution_65536";
  

  const float dt = 0.01f; // Time step
  const int nIters = 10;  // Simulation iterations

  int bytes_A = nBodies_A * sizeof(Body);
  int bytes_B = nBodies_B * sizeof(Body);
  
  float *bufA;
  float *bufB;
 
  
  nvtxRangePushA("cudaMallocManaged"); //NVXT_ANNOTATION
  mallocErr = cudaMallocManaged(&bufA, bytes_A);
  nvtxRangePop(); //NVTX_ANNOTATION
  if(mallocErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(mallocErr));
   
  nvtxRangePushA("cudaMallocManaged"); //NVXT_ANNOTATION
  mallocErr = cudaMallocManaged(&bufB, bytes_B);
  nvtxRangePop(); //NVTX_ANNOTATION
  if(mallocErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(mallocErr));
   
  nvtxRangePushA("cudaMemPrefechAsync"); //NVXT_ANNOTATION
  cudaMemPrefetchAsync(bufA, bytes_A, deviceId);
  nvtxRangePop(); //NVTX_ANNOTATION
  
  nvtxRangePushA("cudaMemPrefechAsync"); //NVXT_ANNOTATION
  cudaMemPrefetchAsync(bufB, bytes_B, deviceId);
  nvtxRangePop(); //NVTX_ANNOTATION
  
  Body *pA = (Body*)bufA;
  Body *pB = (Body*)bufB;
 
   
  read_values_from_file(initialized_values_A, bufA, bytes_A);
  read_values_from_file(initialized_values_B, bufB, bytes_B);
  
  double totalTimeA = 0.0;
  double totalTimeB = 0.0;
  
  /*
   * This simulation will run for 10 cycles of time, calculating gravitational
   * interaction amongst bodies, and adjusting their positions to reflect.
   */

  for (int iter = 0; iter < nIters; iter++) {

    /*if(iter%10 ==0){ //NVTX_DEBUG

        cudaMemPrefetchAsync(buf, bytes, cudaCpuDeviceId);
        write_iteration_to_file(iter, buf, bytes);
    } */

       
   StartTimer();
   computeNBody11(pA,dt,nBodies_A,threadsPerBlock, numberOfBlocks);
   const double tElapsedA = GetTimer() / 1000.0;
   totalTimeA += tElapsedA;
   
   
   StartTimer();
   computeNBody15(pB,dt,nBodies_B, threadsPerBlock, numberOfBlocks);
   const double tElapsedB = GetTimer() / 1000.0;
   totalTimeB += tElapsedB;
  }

  nvtxRangePushA("cudaMemPrefechAsync"); //NVXT_ANNOTATION
  cudaMemPrefetchAsync(bufA, bytes_A, cudaCpuDeviceId);
  nvtxRangePop(); //NVTX_ANNOTATION
  
  nvtxRangePushA("cudaMemPrefechAsync"); //NVXT_ANNOTATION
  cudaMemPrefetchAsync(bufB, bytes_B, cudaCpuDeviceId);
  nvtxRangePop(); //NVTX_ANNOTATION
  
  double avgTimeA = totalTimeA / (double)(nIters);
  float billionsOfOpsPerSecondA = 1e-9 * nBodies_A * nBodies_A / avgTimeA;
  
  double avgTimeB = totalTimeB / (double)(nIters);
  float billionsOfOpsPerSecondB = 1e-9 * nBodies_B * nBodies_B / avgTimeB;
  
  write_values_to_file(solution_values_A, bufA, bytes_A); 
  write_values_to_file(solution_values_B, bufB, bytes_B); 
  
  
  // You will likely enjoy watching this value grow as you accelerate the application,
  // but beware that a failure to correctly synchronize the device might result in
  // unrealistically high values.
  printf("Problem A: %0.3f Billion Interactions / second\n", billionsOfOpsPerSecondA);
  printf("Problem B: %0.3f Billion Interactions / second\n", billionsOfOpsPerSecondB);
  
  cudaFree(bufA);
  cudaFree(bufB);
  nvtxRangePop(); //NVTX_ANNOTATION
}
