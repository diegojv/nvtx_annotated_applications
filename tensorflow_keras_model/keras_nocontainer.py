#!/usr/bin/env python
import nvtx
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import mixed_precision

print("TensorFlow version:", tf.__version__)
policy = tf.keras.mixed_precision.experimental.Policy('float32')
tf.keras.mixed_precision.experimental.set_policy(policy)


print('Compute dtype: %s' % policy.compute_dtype)
print('Variable dtype: %s' % policy.variable_dtype)

@nvtx.annotate("main()")
def main():
    with nvtx.annotate("keras.input"):
        inputs = keras.Input(shape=(784,), name='digits')
    if tf.config.list_physical_devices('GPU'):
        print('The model will run with 4096 units on a GPU')
        num_units = 4096
    else:
        # Use fewer units on CPUs so the model finishes in a reasonable amount of time
        print('The model will run with 64 units on a CPU')
        num_units = 64
    with nvtx.annotate("layers.Dense"):
        dense1 = layers.Dense(num_units, activation='relu', name='dense_1')
        x = dense1(inputs)
        dense2 = layers.Dense(num_units, activation='relu', name='dense_2')
        x = dense2(x) 
        
        #print(dense1.dtype_policy)
        print('x.dtype: %s' % x.dtype.name)
        # 'kernel' is dense1's variable
        print('dense1.kernel.dtype: %s' % dense1.kernel.dtype.name)
        
        # CORRECT: softmax and model output are float32
        x = layers.Dense(10, name='dense_logits')(x)
        outputs = layers.Activation('softmax', dtype='float32', name='predictions')(x)
        print('Outputs dtype: %s' % outputs.dtype.name)
        
       # The linear activation is an identity function. So this simply casts 'outputs'
       # to float32. In this particular case, 'outputs' is already float32 so this is a
       # no-op.
        outputs = layers.Activation('linear', dtype='float32')(outputs)
    with nvtx.annotate("keras.Model"):
       model = keras.Model(inputs=inputs, outputs=outputs)
    with nvtx.annotate("model.compile"):
       model.compile(loss='sparse_categorical_crossentropy',
               optimizer=keras.optimizers.RMSprop(),
               metrics=['accuracy'])
    with nvtx.annotate("mnist.load_data"):
        (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
        x_train = x_train.reshape(60000, 784).astype('float32') / 255
        x_test = x_test.reshape(10000, 784).astype('float32') / 255
    
    with nvtx.annotate("model.get_weights"):
        initial_weights = model.get_weights()
    with nvtx.annotate("model.fit"):
        history = model.fit(x_train, y_train,
                            batch_size=8192,
                            epochs=10,
                            validation_split=0.2)
    with nvtx.annotate("model.evaluate"):
        test_scores = model.evaluate(x_test, y_test, verbose=2)
    
    print('Test loss:', test_scores[0])
    print('Test accuracy:', test_scores[1])


main()
